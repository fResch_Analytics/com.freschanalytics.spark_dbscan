package com.freschanalytics.spark_dbscan.dbscan.util.commandLine

import com.freschanalytics.spark_dbscan.dbscan.DbscanSettings

private [dbscan] trait EpsArg {
  var eps: Double = DbscanSettings.getDefaultEpsilon
}

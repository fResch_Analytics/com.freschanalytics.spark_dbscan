name := "com.freschanalytics.spark_dbscan"

organization := "com.freschanalytics"

version := "0.1"

scalaVersion := "2.11.8"

libraryDependencies += "org.apache.spark" %% "spark-core" % "2.3.3" % "provided"
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.8"
libraryDependencies += "org.scalatest" % "scalatest_2.11" % "3.0.8" % Test
libraryDependencies += "org.apache.commons" % "commons-math3" % "3.2"
libraryDependencies += "com.github.scopt" %% "scopt" % "3.7.0"

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}
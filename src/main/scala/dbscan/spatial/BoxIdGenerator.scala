package com.freschanalytics.spark_dbscan.dbscan.spatial

import com.freschanalytics.spark_dbscan.dbscan._

private [dbscan] class BoxIdGenerator (val initialId: BoxId) {
  var nextId = initialId

  def getNextId (): BoxId = {
    nextId += 1
    nextId
  }
}

package com.freschanalytics.spark_dbscan.dbscan.util.commandLine

import com.freschanalytics.spark_dbscan.dbscan.spatial.rdd.PartitioningSettings


private [dbscan] trait NumberOfPointsInPartitionArg {
  var numberOfPoints: Long = PartitioningSettings.DefaultNumberOfPointsInBox
}

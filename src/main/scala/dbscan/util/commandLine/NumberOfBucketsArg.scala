package com.freschanalytics.spark_dbscan.dbscan.util.commandLine

import com.freschanalytics.spark_dbscan.dbscan.exploratoryAnalysis.ExploratoryAnalysisHelper

private [dbscan] trait NumberOfBucketsArg {
  var numberOfBuckets: Int = ExploratoryAnalysisHelper.DefaultNumberOfBucketsInHistogram
}

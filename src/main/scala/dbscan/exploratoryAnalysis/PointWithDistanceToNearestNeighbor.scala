package com.freschanalytics.spark_dbscan.dbscan.exploratoryAnalysis

import com.freschanalytics.spark_dbscan.dbscan.spatial.Point

private [dbscan] class PointWithDistanceToNearestNeighbor (pt: Point, d: Double = Double.MaxValue) extends  Point (pt) {
  var distanceToNearestNeighbor = d
}
